
var font;
function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
  //font = loadFont('FontAwesome.otf');
  //font = loadFont('FiraSans-Medium.ttf');
}

var BOX_WIDTH=10
var BOX_SEP=BOX_WIDTH

var L = 5
var A = L+5
var B = A+6
var CUBES_WIDTH = B+2
var CUBES_HEIGHT = 5
var CUBES_Y_SEP = 2
var cubes = [
  // ERRE
  [0,0],
  [1,0],
  [2,0],
  [3,0],
  [4,0],
  [5,0],
  [5,1],
  [5,2],
  [5,3],
  [4,2],
  [3,1],
  [2,2],
  [1,3],
  [0,3],
  // ELE
  [0,L+0],
  [1,L+0],
  [2,L+0],
  [3,L+0],
  [4,L+0],
  [5,L+0],
  [0,L+1],
  [0,L+2],
  [0,L+3],
  // A
  [0,A+0],
  [1,A+0],
  [2,A+1],
  [3,A+1],
  [4,A+2],
  [5,A+2],
  [3,A+3],
  [2,A+3],
  [1,A+4],
  [0,A+4],
  [2,A+2],
  // BE
  [0,B+0],
  [1,B+0],
  [2,B+0],
  [3,B+0],
  [4,B+0],
  [5,B+0],
  [5,B+1],
  [5,B+2],
  [4,B+3],
  [3,B+2],
  [3,B+1],
  [2,B+1],
  [2,B+2],
  [1,B+3],
  [0,B+1],
  [0,B+2],
]

//var INIT_SPEED=0.097
var INIT_SPEED=0.047
var SPEED_TOP = 0.025
var SPEED_STEP = 0.00005

var rotSpeed = INIT_SPEED
var goingClock = true

function draw() {
  //background(100);
  background(0);
  //background(255);

  // enderezar RLAB horizontal
  rotateZ(-PI/2);

  /*if (goingClock){
    rotSpeed += 0.00005
    if (rotSpeed >= SPEED_TOP){
      goingClock = false
      console.log('changed clock')
    }
  }else{
    rotSpeed -= 0.00005
    if (rotSpeed <= -SPEED_TOP){
      goingClock = true
      console.log('changed clock')
    }
  }*/
  //rotateX(sin(frameCount / TWO_PI) * 30 * rotSpeed);
  //rotateZ(frameCount * rotSpeed);
  //rotateY(-frameCount * 0.007);
  //rotateZ(-PI/2+-frameCount * 0.001);
  //rotateY(PI);
  //rotateZ(-frameCount * rotSpeed);
  //translate(0,0,-100)

  // nice random rotation
  rotateX(frameCount * rotSpeed/2);
  rotateZ(frameCount * rotSpeed/2);

  // rotar de adelante hacia atrás
  // hacia atrás
  //  rotateY(frameCount * rotSpeed);
  // hacia adelante
  rotateY(-frameCount * rotSpeed*2);

  // rotar horizontalmente (tipo diamante)
  //rotateX(frameCount * rotSpeed);

  // axis
  function drawAxis(){
    strokeWeight(2)
    stroke('red')
    line(0, 0, 0, 100, 0, 0)
    stroke('green')
    line(0, 0, 0, 0, 100, 0)
    stroke('blue')
    line(0, 0, 0, 0, 0, 100)
  }
  //drawAxis()

  strokeWeight(1)
  stroke(0)
  /*for (var i = 0; i < 20; i++) {
    push()
    translate(i*BOX_SEP, 0, 0)
    box(BOX_WIDTH);
    pop()
  }*/

  var offsetY = - CUBES_WIDTH * BOX_SEP / 2
  var offsetX = - CUBES_HEIGHT * BOX_SEP / 2
  //scale(.5,.5,.5)
  scale(1.5,1.5,1.5)

  //var cubesLinesRadius = 20
  var cubesLinesRadius = 2
  for (var i = 0; i < cubesLinesRadius*2+1; i++) {
    drawCubesR(
      x=> (CUBES_HEIGHT+CUBES_Y_SEP)*BOX_SEP * (-cubesLinesRadius+i) + offsetX + x*BOX_SEP,
      y=>offsetY + y*BOX_SEP
    )
  }
  push()
  translate(0,CUBES_WIDTH * (BOX_SEP+2))
  for (var i = 0; i < cubesLinesRadius*2+1; i++) {
    drawCubesG(
      x=> (CUBES_HEIGHT+CUBES_Y_SEP)*BOX_SEP * (-cubesLinesRadius+i) + offsetX + x*BOX_SEP,
      y=>offsetY + y*BOX_SEP
    )
  }
  pop()
  push()
  translate(0,-CUBES_WIDTH * (BOX_SEP+2))
  for (var i = 0; i < cubesLinesRadius*2+1; i++) {
    drawCubesB(
      x=> (CUBES_HEIGHT+CUBES_Y_SEP)*BOX_SEP * (-cubesLinesRadius+i) + offsetX + x*BOX_SEP,
      y=>offsetY + y*BOX_SEP
    )
  }
  pop()//*/
  /*drawCubes(
    x=>offsetX + x*BOX_SEP,
    y=>offsetY + y*BOX_SEP
  )
  drawCubes(
    x=> (CUBES_HEIGHT+CUBES_Y_SEP)*BOX_SEP + offsetX + x*BOX_SEP,
    y=>offsetY + y*BOX_SEP
  )
  drawCubes(
    x=> (CUBES_HEIGHT+CUBES_Y_SEP)*BOX_SEP * 2 + offsetX + x*BOX_SEP,
    y=>offsetY + y*BOX_SEP
  )
  drawCubes(
    x=>-(CUBES_HEIGHT+CUBES_Y_SEP)*BOX_SEP + offsetX + x*BOX_SEP,
    y=>offsetY + y*BOX_SEP
  )
  drawCubes(
    x=>-(CUBES_HEIGHT+CUBES_Y_SEP)*BOX_SEP * 2 + offsetX + x*BOX_SEP,
    y=>offsetY + y*BOX_SEP
  )*/

  /*drawCubesInv(
    x=>offsetX + x*BOX_SEP,
    y=>offsetY + y*BOX_SEP
  )
  drawCubesInv(
    x=> (CUBES_HEIGHT+CUBES_Y_SEP)*BOX_SEP + offsetX + x*BOX_SEP,
    y=>offsetY + y*BOX_SEP
  )
  drawCubesInv(
    x=> (CUBES_HEIGHT+CUBES_Y_SEP)*BOX_SEP * 2 + offsetX + x*BOX_SEP,
    y=>offsetY + y*BOX_SEP
  )
  drawCubesInv(
    x=>-(CUBES_HEIGHT+CUBES_Y_SEP)*BOX_SEP + offsetX + x*BOX_SEP,
    y=>offsetY + y*BOX_SEP
  )
  drawCubesInv(
    x=>-(CUBES_HEIGHT+CUBES_Y_SEP)*BOX_SEP * 2 + offsetX + x*BOX_SEP,
    y=>offsetY + y*BOX_SEP
  )*/
}

var redBase = 155, redRand = 100
var colBaseInv = 155, colRandInv = 100
var randColChance = 0.03

var COLOR_R = 1,  COLOR_B = 2,  COLOR_G = 3;
function drawCubesR(fx, fy){
  if (random()<randColChance)
    drawCubes(fx, fy, random()<0.5?COLOR_G:COLOR_B)
  else
    drawCubes(fx, fy, COLOR_R)
}
function drawCubesG(fx, fy){
  if (random()<randColChance)
    drawCubes(fx, fy, random()<0.5?COLOR_R:COLOR_B)
  else
    drawCubes(fx, fy, COLOR_G)
}
function drawCubesB(fx, fy){
  if (random()<randColChance)
    drawCubes(fx, fy, random()<0.5?COLOR_G:COLOR_R)
  else
    drawCubes(fx, fy, COLOR_B)
}

function drawCubes(fx, fy, color){
  if (!fx) fx = () => 0
  if (!fy) fy = () => 0
  cubes.forEach(c=>{
    var [x,y]=c
    push()

    translate(fx(x), fy(y), 0)

    //console.log('draw box', c, x*BOX_SEP, y*BOX_SEP, z*BOX_SEP)
    //fill(redBase+random()*redRand,0,0)
    //fill(0,redBase+random()*redRand,0)
    switch (color) {
      case COLOR_R:
        fill(redBase+random()*redRand,0,0)
        break;
      case COLOR_G:
        fill(0,redBase+random()*redRand,0)
        break;
      case COLOR_B:
        fill(0,0,redBase+random()*redRand)
        break;
      default:
        throw new Error('Bad color')
    }
    box(BOX_WIDTH);
    pop()
  })
}

function drawCubesInv(fx, fy){
  if (!fx) fx = () => 0
  if (!fy) fy = () => 0
  cubes.forEach(c=>{
    var [x,y]=c
    push()
    translate(0, fy(y), fx(x))
    //console.log('draw box', c, x*BOX_SEP, y*BOX_SEP, z*BOX_SEP)
    fill(0,colBaseInv+random()*colRandInv,0)
    box(BOX_WIDTH);
    pop()
  })
}
