let gridTopX;
let gridTopY;
const sideLength = 25;

const CUBES = 200
const SPEED_FRAME = 2
const STEP = 1.2
const RED = 255
const GREEN = 255
const BLUE = 255
/*const RED = 255
const GREEN = 0
const BLUE = 20*/
const BG = 20

const cubes = [];

function setup() {
  createCanvas(windowWidth, windowHeight);
  gridTopX = width / 2;
  //gridTopY = height / 2;
  gridTopY = 2*height / 3;

  strokeWeight(2);

  cubes.push(new Cube(0, 0, 0));

  while (cubes.length < CUBES) {
    addRandomCube();
  }

  // Sort so the cubes are drawn in the right order
  cubes.sort((a, b) => {
    return a.getSortString().localeCompare(b.getSortString());
  });
}

function draw() {
  background(BG);

  drawAxis()

  for (const cube of cubes) {
    cube.draw();
  }

  /*if (frameCount%SPEED_FRAME==0){
    randomizeCubes()
  }//*/

  translate(0, height/2-5)
  textSize(30);
  textAlign(CENTER);
  fill(0)
  text(frameCount, width / 2, height / 2);
}

function addRandomCube() {
  let cubeAdded = false;

  while (!cubeAdded) {
    const randomCube = random(cubes);

    let newCubeC = randomCube.c;
    let newCubeR = randomCube.r;
    let newCubeZ = randomCube.z;

    const r = random(1);
    if (r < .3) {
      newCubeC+=STEP;
    } else if (r < .6) {
      newCubeR+=STEP;
    } else {
      newCubeZ+=STEP;
    }

    const spotTaken = cubes.some((cube) => {
      return cube.c == newCubeC &&
        cube.r == newCubeR &&
        cube.z == newCubeZ;
    });

    if (!spotTaken) {
      cubes.push(new Cube(newCubeC, newCubeR, newCubeZ));
      cubeAdded = true;
    }
  }
}

class Cube {

  constructor(c, r, z) {
    this.c = c;
    this.r = r;
    this.z = z;
    this.red = random(RED);
    this.green = random(GREEN);
    this.blue = random(BLUE);
  }

  draw() {
    const x = gridTopX + (this.c - this.r) * sideLength *
      sqrt(3) / 2;
    const y = gridTopY + (this.c + this.r) * sideLength / 2 -
      (sideLength * this.z);

    const points = [];
    for (let angle = PI / 6; angle < PI * 2; angle += PI / 3) {
      points.push(
        createVector(x + cos(angle) * sideLength,
          y + sin(angle) * sideLength));
    }

    strokeWeight(0);
    //stroke('purple');
    fill(this.red * .75, this.green * .75, this.blue * .75);
    quad(x, y,
      points[5].x, points[5].y,
      points[0].x, points[0].y,
      points[1].x, points[1].y);

    fill(this.red * .9, this.green * .9, this.blue * .9);
    quad(x, y,
      points[1].x, points[1].y,
      points[2].x, points[2].y,
      points[3].x, points[3].y);

    fill(this.red, this.green, this.blue);
    quad(x, y,
      points[3].x, points[3].y,
      points[4].x, points[4].y,
      points[5].x, points[5].y);
  }

  getSortString() {
    return this.z + '.' + this.r + '.' + this.c;
  }
}

function drawAxis() {
  strokeWeight(4);
  stroke('purple');
  line(...coords(0,0,0), ...coords(100,0,0))
  stroke('red');
  line(...coords(0,0,0), ...coords(0,100,0))
  stroke('salmon');
  line(...coords(0,0,0), ...coords(0,0,100))
}

function drawCRZ(c,r,z) {
  const x = gridTopX + (c - r) * sideLength *
    sqrt(3) / 2;
  const y = gridTopY + (c + r) * sideLength / 2 -
    (sideLength * z);

  strokeWeight(10);
  stroke('purple');
  point(x,y)
}

function crzToXy(c,r,z){
  const x = gridTopX + (c - r) * sideLength *
    sqrt(3) / 2;
  const y = gridTopY + (c + r) * sideLength / 2 -
    (sideLength * z);
  return [x, y]
}
// alias
var coords = crzToXy
